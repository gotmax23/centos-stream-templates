# Merge Request Required Information

## Summary of Changes


## Approved Bugzilla Ticket
All submissions to CentOS Stream must reference an approved ticket in [Red Hat Bugzilla](https://bugzilla.redhat.com/). Please follow the CentOS Stream [contribution documentation](https://docs.centos.org/en-US/stream-contrib/) for how to file this ticket and have it approved.
